package com.newtours.qaf.beans;

import com.qmetry.qaf.automation.data.BaseDataBean;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;
import com.qmetry.qaf.automation.util.Randomizer;

public class RegisterBean extends BaseDataBean {

	@Randomizer(length = 15, type= RandomizerTypes.LETTERS_ONLY)
	String Fname;

	@Randomizer(length = 15, type= RandomizerTypes.LETTERS_ONLY)
	String Lname;
	
	@Randomizer(length = 10, type= RandomizerTypes.DIGITS_ONLY)
	String phone;
	
	@Randomizer(length = 15, type= RandomizerTypes.MIXED, suffix="gmail.com")
	String email;
	
	@Randomizer(length = 25, type= RandomizerTypes.MIXED)
	String address;
	
	@Randomizer(length = 10, type= RandomizerTypes.LETTERS_ONLY)
	String city;
	
	@Randomizer(length = 10, type= RandomizerTypes.LETTERS_ONLY)
	String state;
	
	@Randomizer(length = 6, type= RandomizerTypes.DIGITS_ONLY)
	String postalcode;
	
	
	
	public String getFname() {
		return Fname;
	}
	
	public String getLname() {
		return Lname;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getState() {
		return state;
	}
	
	public String getPostalcode() {
		return postalcode;
	}
	
	
}
