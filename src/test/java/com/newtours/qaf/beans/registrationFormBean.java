package com.newtours.qaf.beans;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.ui.annotations.UiElement;
import com.qmetry.qaf.automation.ui.annotations.UiElement.Type;
import com.qmetry.qaf.automation.util.Randomizer;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;

public class registrationFormBean extends BaseFormDataBean {

	@Randomizer(length = 15, type = RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc = "registerpage.txt.fname")
	private String Fname;

	@Randomizer(length = 15, type = RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc = "registerpage.txt.lname")
	private String Lname;

	@Randomizer(length = 10, type = RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc = "registerpage.txt.phone")
	private String phone;

	@Randomizer(length = 15, type = RandomizerTypes.MIXED, suffix = "@gmail.com")
	@UiElement(fieldLoc = "registerpage.txt.email")
	private String email;

	@Randomizer(length = 25, type = RandomizerTypes.MIXED)
	@UiElement(fieldLoc = "registerpage.txt.address")
	private String address;

	@Randomizer(length = 10, type = RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc = "registerpage.txt.city")
	private String city;

	@Randomizer(length = 10, type = RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc = "registerpage.txt.state")
	private String state;

	@Randomizer(length = 6, type = RandomizerTypes.DIGITS_ONLY)
	@UiElement(fieldLoc = "registerpage.txt.postalcode")
	private String postalcode;

	@Randomizer(length = 10, type = RandomizerTypes.LETTERS_ONLY)
	@UiElement(fieldLoc = "registerpage.txt.country", fieldType = Type.selectbox)
	private String country;

	@Randomizer(length = 10, type = RandomizerTypes.MIXED)
	@UiElement(fieldLoc = "registerpage.txt.username", fieldType = Type.selectbox)
	private String username;

	@Randomizer(length = 10, type = RandomizerTypes.MIXED)
	@UiElement(fieldLoc = "registerpage.txt.password", fieldType = Type.selectbox)
	private String password;

	@Randomizer(length = 10, type = RandomizerTypes.MIXED)
	@UiElement(fieldLoc = "registerpage.txt.cnfmpassword", fieldType = Type.selectbox)
	private String cnfmpassword;

	public String getFname() {
		return Fname;
	}

	public String getLname() {
		return Lname;
	}

	public String getPhone() {
		return phone;
	}

	public String getEmail() {
		return email;
	}

	public String getAddress() {
		return address;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public String getCountry() {
		return country;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getCnfmPassword() {
		return cnfmpassword;
	}

}
