package com.newtours.qaf.tests;

import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.newtours.qaf.pages.HomePage;
import com.newtours.qaf.pages.SignOnPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class SignOnPageTest extends WebDriverTestCase {

	HomePage homepage;
	SignOnPage signon_page;

	@BeforeClass
	public void beforeClass() {

		homepage = new HomePage();
		signon_page = homepage.gotoSignOnPage();
	}

	@QAFDataProvider(key = "login.data")
	@Test(description = "Verify invalid login with invalid username and password")
	public void verifyInvalidLogin(Map<String, String> data) {
	
		signon_page.InavlidLogin(data.get("user_name"), data.get("password"));
		signon_page.verifyTitle();

	}

	@AfterClass
	public void afterClass() {
		Reporter.log("Test case passed successfully");
	}

}
