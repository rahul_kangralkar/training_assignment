package com.newtours.qaf.tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.newtours.qaf.pages.FlightFinderPage;
import com.newtours.qaf.pages.HomePage;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class HomePageTest extends WebDriverTestCase {

	HomePage homepage;
	FlightFinderPage flightfinderpage;

	@BeforeClass
	public void setup() {
		homepage = new HomePage();
	}

	@Test(description = "Verify all the links are present at Left menu of Login Page", priority = 1)
	public void verifyLeftMenu() {

		homepage.verifyLeftMenu();
	}

	@Test(description = "Verify all the links are present at top menu of Login Page", priority = 2)
	public void verifyTopMenu() {

		homepage.verifyTopMenu();

	}

	@Test(description = "Verify different sections available on Login Page", priority = 3)
	public void verifyDifferentSections() {

		homepage.verifyDifferentSections();

	}

	@Test(description = "Verify user is able to sign in susseccfully from Login Page", priority = 4)
	public void verifyValidUserLogin() {
		
		String userID = ConfigurationManager.getBundle().getString("valid.username");
		String password = ConfigurationManager.getBundle().getString("valid.password");
		flightfinderpage = homepage.validLogin(userID, password);
		flightfinderpage.verifyRoundTripBtn();
		
	}

	@AfterClass
	public void tearDown() {

		Reporter.log("HomePageTest completed successfully");

	}

}
