package com.newtours.qaf.tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.newtours.qaf.pages.BookAFlightPage;
import com.newtours.qaf.pages.FlightConfirmationPage;
import com.newtours.qaf.pages.FlightFinderPage;
import com.newtours.qaf.pages.HomePage;
import com.newtours.qaf.pages.SelectFlightsPage;
import com.newtours.qaf.pages.SignOnPage;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class FlightConfirmationPageTest extends WebDriverTestCase {

	HomePage homepage;
	FlightFinderPage flightfinder_page;
	SelectFlightsPage selectflight_page;
	BookAFlightPage bookflight_page;
	FlightConfirmationPage flightconfirmation_page;
	SignOnPage signon_page;

	@BeforeClass
	public void setup() {

		homepage = new HomePage();
		String userID = ConfigurationManager.getBundle().getString("valid.username");
		String password = ConfigurationManager.getBundle().getString("valid.password");
		flightfinder_page = homepage.validLogin(userID, password);
		selectflight_page = flightfinder_page.gotoSelectFlightsPage();
		bookflight_page = selectflight_page.gotoBookAFlightPage();
		flightconfirmation_page = bookflight_page.gotoFlightConfirmationPage();
		
	}

	@Test(description = "Verify the navigation on 'Log Out' button on Flight Confirmation Page(QEO-14000)", priority = 1)
	public void verifyLogoutBtn() {

		signon_page = flightconfirmation_page.gotoSignOnPage();
		signon_page.verifyTitle();

	}

	@AfterClass
	public void tearDown() {
		Reporter.log("Test case completed successfully");

	}

}
