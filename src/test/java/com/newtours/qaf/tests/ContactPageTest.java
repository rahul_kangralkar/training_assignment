package com.newtours.qaf.tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.newtours.qaf.pages.ContactPage;
import com.newtours.qaf.pages.HomePage;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class ContactPageTest extends WebDriverTestCase {

	HomePage homepage;
	ContactPage contactpage;

	@BeforeClass
	public void setup() {
		homepage = new HomePage();
		contactpage = homepage.goToContactPage();
	}

	@Test(description = "Verify 'Under Construction' message is displayed at Contact Page(QEO-14002)", priority = 1)
	public void verifyMessage() {

		Validator.assertTrue(
				contactpage.getMessageElement().getText()
						.contains("This section of our web site is currently under construction."),
				"Message is incorrect", "Message is correct");

	}

	@Test(description = "Verify 'Sorry for any inconvienece' message is displayed at Contact Page(QEO-14002)", priority = 2)
	public void verifySorryMessage() {
		Validator.assertTrue(contactpage.getSorryMessageEelemnt().getText().contains("Sorry for any inconvienece."),
				"Message is incorrect", "Message is correct");
	}

	@Test(description = "Verify homepage is displayed when clicked on 'BACK TO HOME' button(QEO-14002)", priority = 3)
	public void verifyBackToHomeBtn() {

		contactpage.getBackToHomeBtn().click();
		homepage.verifyDifferentSections();

	}

	@AfterClass
	public void tearDown() {

		Reporter.log("HomePageTest completed successfully");

	}

}
