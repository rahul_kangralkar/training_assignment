package com.newtours.qaf.tests;

import java.util.Map;

import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.newtours.qaf.pages.HomePage;
import com.newtours.qaf.pages.RegistrationPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;

public class RegistrationPageTest {

	HomePage homepage;
	RegistrationPage register_page;

	@BeforeMethod
	public void beforeClass() {
		homepage = new HomePage();
		register_page = homepage.gotoRegisterPage();

	}

	@Test(description = "Verify user registration by filling all valid data.(QEO-14003)", priority = 2)
	public void verifyValidRegistration() {

		register_page.fillRegistrationData();
		register_page.getSubmitBtn().click();
		register_page.verifyGreetingMessage();
		

	}

	@QAFDataProvider(dataFile = "resources/TestData/InvalidRegistrationData.xls", sheetName = "InvalidData", key = "Register")
	@Test(description = "Verify user registration with some invalid data.(QEO-14004)", priority = 1)
	public void verifyInvalidRegistration(Map<String, String> data) {

		register_page.getFname().sendKeys(data.get("Firstname"));
		register_page.getLname().sendKeys(data.get("Lastname"));
		register_page.getPhone().sendKeys(data.get("Phone"));
		register_page.getEmail().sendKeys(data.get("Email"));
		register_page.getSubmitBtn().click();
		register_page.getGreetingMsg().verifyText(
				"Thank you for registering. You may now sign-in using the user name and password you've just entered.");
		homepage.getRegisterLink().click();

	}

	@AfterMethod
	public void afterClass() {
		Reporter.log("Registration Page Test successfully completed");
	}

}
