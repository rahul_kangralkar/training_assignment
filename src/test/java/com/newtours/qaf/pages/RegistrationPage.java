package com.newtours.qaf.pages;

import com.newtours.qaf.beans.registrationFormBean;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class RegistrationPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "registerpage.txt.fname")
	QAFWebElement Fname;

	@FindBy(locator = "registerpage.txt.lname")
	QAFWebElement Lname;

	@FindBy(locator = "registerpage.txt.phone")
	QAFWebElement phone;

	@FindBy(locator = "registerpage.txt.email")
	QAFWebElement email;

	@FindBy(locator = "registerpage.txt.address")
	QAFWebElement address;

	@FindBy(locator = "registerpage.txt.city")
	QAFWebElement city;

	@FindBy(locator = "registerpage.txt.state")
	QAFWebElement state;

	@FindBy(locator = "registerpage.txt.postalcode")
	QAFWebElement postalcode;

	@FindBy(locator = "registerpage.txt.country")
	QAFWebElement country;

	@FindBy(locator = "registerpage.txt.username")
	QAFWebElement username;

	@FindBy(locator = "registerpage.txt.password")
	QAFWebElement password;

	@FindBy(locator = "registerpage.txt.cnfmpassword")
	QAFWebElement cnfmpassword;

	@FindBy(locator = "registerpage.txt.submitBtn")
	QAFWebElement submitBtn;

	@FindBy(locator = "registerpage.txt.greetings")
	QAFWebElement greetings;

	@FindBy(locator = "registerpage.txt.greetingMsg")
	QAFWebElement greetingMsg;

	public QAFWebElement getSubmitBtn() {
		return submitBtn;
	}
	
	public QAFWebElement getFname() {
		return Fname;
	}
	
	public QAFWebElement getLname() {
		return Lname;
	}
	
	public QAFWebElement getPhone() {
		return phone;
	}
	
	public QAFWebElement getEmail() {
		return email;
	}
	
	public QAFWebElement getGreetingMsg() {
		return greetingMsg;
	}

	public void fillRegistrationData() {

		registrationFormBean rBean = new registrationFormBean();
		// rBean.fillRandomData();
		rBean.fillFromConfig("data.register.user");
		rBean.fillUiElements();
	}

	public void verifyGreetingMessage() {

		greetings.verifyText("Dear Elena Gilbert,");
		greetingMsg.verifyText(
				"Thank you for registering. You may now sign-in using the user name and password you've just entered.");

	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");

	}

}
