package com.newtours.qaf.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class SelectFlightsPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "selectflightspage.btn.continueBtn")
	private QAFWebElement ContinueBtn;

	public BookAFlightPage gotoBookAFlightPage() {
		ContinueBtn.click();
		return new BookAFlightPage();

	}

	public QAFWebElement getContinueBtn() {
		return ContinueBtn;
	}

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");

	}

}
