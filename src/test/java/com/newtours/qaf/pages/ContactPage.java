package com.newtours.qaf.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ContactPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "contactpage.txt.AlertMessage")
	private QAFWebElement message;

	@FindBy(locator = "contactpage.txt.SorryMessage")
	private QAFWebElement sorryMessage;

	@FindBy(locator = "contactpage.btn.BackToHomeBtn")
	private QAFWebElement BackToHomeBtn;

	public QAFWebElement getMessageElement() {
		return message;
	}

	public QAFWebElement getSorryMessageEelemnt() {
		return sorryMessage;
	}

	public QAFWebElement getBackToHomeBtn() {
		return BackToHomeBtn;
	}

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");
	}
}
