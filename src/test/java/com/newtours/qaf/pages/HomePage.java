package com.newtours.qaf.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	// Locators
	@FindBy(locator = "homepage.txt.username")
	private QAFWebElement userIDField;

	@FindBy(locator = "homepage.txt.password")
	private QAFWebElement passwordField;

	@FindBy(locator = "homepage.btn.signin")
	private QAFWebElement loginBtn;

	@FindBy(locator = "homepage.link.homeLink")
	private QAFWebElement homeLink;

	@FindBy(locator = "homepage.link.flightsLink")
	private QAFWebElement flightsLink;

	@FindBy(locator = "homepage.link.HotelsLink")
	private QAFWebElement HotelsLink;

	@FindBy(locator = "homepage.link.CarRentalsLink")
	private QAFWebElement CarRentalsLink;

	@FindBy(locator = "homepage.link.cruisesLink")
	private QAFWebElement cruisesLink;

	@FindBy(locator = "homepage.link.destinationsLink")
	private QAFWebElement destinationsLink;

	@FindBy(locator = "homepage.link.vacationsLink")
	private QAFWebElement vacationsLink;

	@FindBy(locator = "homepage.link.signOnLink")
	private QAFWebElement signOnLink;

	@FindBy(locator = "homepage.link.registerLink")
	private QAFWebElement registerLink;

	@FindBy(locator = "homepage.link.supportLink")
	private QAFWebElement supportLink;

	@FindBy(locator = "homepage.link.contactLink")
	private QAFWebElement contactLink;

	@FindBy(locator = "homepage.img.FeaturedDestination")
	private QAFWebElement FeaturedDestinationImg;

	@FindBy(locator = "homepage.img.Specials")
	private QAFWebElement SpecialsImg;

	@FindBy(locator = "homepage.img.TourTips")
	private QAFWebElement TourTipsImg;

	@FindBy(locator = "homepage.img.FindAFlight")
	private QAFWebElement FindAFlightImg;

	@FindBy(locator = "homepage.img.Destinations")
	private QAFWebElement DestinationsImg;

	@FindBy(locator = "homepage.img.Vacations")
	private QAFWebElement VacationsImg;

	@FindBy(locator = "homepage.img.Register")
	private QAFWebElement RegisterImg;

	@FindBy(locator = "homepage.img.Links")
	private QAFWebElement LinksImg;

	// Functions

	public QAFWebElement getFeaturedDestinationImg() {
		return signOnLink;
	}

	public QAFWebElement getSpecialsImg() {
		return signOnLink;
	}

	public QAFWebElement getTourTipsImg() {
		return signOnLink;
	}

	public QAFWebElement getFindAFlightImg() {
		return signOnLink;
	}

	public QAFWebElement getDestinationsImg() {
		return signOnLink;
	}

	public QAFWebElement getVacationsImg() {
		return signOnLink;
	}

	public QAFWebElement getRegisterImg() {
		return signOnLink;
	}

	public QAFWebElement getLinksImg() {
		return signOnLink;
	}

	public QAFWebElement getSignOnLink() {
		return signOnLink;
	}

	public QAFWebElement getRegisterLink() {
		return registerLink;
	}

	public QAFWebElement getSupportLink() {
		return supportLink;
	}

	public QAFWebElement getContactLink() {
		return contactLink;
	}

	public QAFWebElement getUserIDfield() {
		return userIDField;
	}

	public QAFWebElement getPasswordField() {
		return passwordField;
	}

	public QAFWebElement getLoginBtn() {
		return loginBtn;
	}

	public QAFWebElement getHomeLink() {
		return homeLink;
	}

	public QAFWebElement getFlightsLink() {
		return flightsLink;
	}

	public QAFWebElement getHotelsLink() {
		return HotelsLink;
	}

	public QAFWebElement getCarRentalsLink() {
		return CarRentalsLink;
	}

	public QAFWebElement getCruisesLink() {
		return cruisesLink;
	}

	public QAFWebElement getDestinationsLink() {
		return destinationsLink;
	}

	public QAFWebElement getVacationsLink() {
		return vacationsLink;
	}

	public ContactPage goToContactPage() {
		contactLink.click();
		return new ContactPage();
	}

	public FlightFinderPage validLogin(String userID, String password) {
		userIDField.sendKeys(userID);
		passwordField.sendKeys(password);
		loginBtn.click();
		return new FlightFinderPage();
	}

	public SignOnPage gotoSignOnPage() {
		signOnLink.click();
		return new SignOnPage();

	}

	public RegistrationPage gotoRegisterPage() {
		registerLink.click();
		return new RegistrationPage();
	}

	public void verifyLeftMenu() {

		Validator.assertTrue(getHomeLink().verifyPresent(), "Homelink is not present", "Homelink is present");
		Validator.assertTrue(getFlightsLink().verifyPresent(), "Flightslink is not present", "Flightslink is present");
		Validator.assertTrue(getHotelsLink().verifyPresent(), "Hotelslink is not present", "Hotelslink is present");
		Validator.assertTrue(getCarRentalsLink().verifyPresent(), "CarRentalsLink is not present",
				"CarRentalsLink is present");
		Validator.assertTrue(getCruisesLink().verifyPresent(), "CruisesLink is not present", "CruisesLink is present");
		Validator.assertTrue(getDestinationsLink().verifyPresent(), "DestinationsLink is not present",
				"DestinationsLink is present");
		Validator.assertTrue(getVacationsLink().verifyPresent(), "VacationsLink is not present",
				"VacationsLink is present");
	}

	public void verifyTopMenu() {
		Validator.assertTrue(getSignOnLink().verifyPresent(), "SignOnLink is not present", "SignOnLink is present");
		Validator.assertTrue(getRegisterLink().verifyPresent(), "RegisterLink is not present",
				"RegisterLink is present");
		Validator.assertTrue(getSupportLink().verifyPresent(), "SupportLink is not present", "SupportLink is present");
		Validator.assertTrue(getContactLink().verifyPresent(), "ContactLink is not present", "ContactLink is present");
	}

	public void verifyDifferentSections() {
		Validator.assertTrue(getFeaturedDestinationImg().verifyPresent(), "FeaturedDestinationImg is not present",
				"FeaturedDestinationImg is present");
		Validator.assertTrue(getSpecialsImg().verifyPresent(), "SpecialsImg is not present", "SpecialsImg is present");
		Validator.assertTrue(getTourTipsImg().verifyPresent(), "TourTipsImg is not present", "TourTipsImg is present");
		Validator.assertTrue(getFindAFlightImg().verifyPresent(), "FindAFlightImg is not present",
				"FindAFlightImg is present");
		Validator.assertTrue(getDestinationsImg().verifyPresent(), "DestinationsImg is not present",
				"DestinationsImg is present");
		Validator.assertTrue(getVacationsImg().verifyPresent(), "VacationsImg is not present",
				"VacationsImg is present");
		Validator.assertTrue(getRegisterImg().verifyPresent(), "RegisterImg is not present", "RegisterImg is present");
		Validator.assertTrue(getLinksImg().verifyPresent(), "LinksImg is not present", "LinksImg is present");
	}

	public HomePage() {
		openPage(pageLocator, launchArguments);
		driver.manage().window().maximize();
	}

	@Override
	public void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");
	}
}
