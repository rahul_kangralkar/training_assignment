package com.newtours.qaf.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class SignOnPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "signonpage.txt.signonheading")
	private QAFWebElement SignOnHeading;

	@FindBy(locator = "signonpage.txt.username")
	private QAFWebElement userIDField;

	@FindBy(locator = "signonpage.txt.password")
	private QAFWebElement passwordField;

	@FindBy(locator = "signonpage.btn.submit")
	private QAFWebElement submitBtn;

	public void verifyTitle() {

		Validator.assertTrue(driver.getTitle().contains("Sign-on: Mercury Tours"), "User is not on Sign-On page",
				"User is on Sign-On page");
	}

	public void InavlidLogin(String userID, String password) {
		userIDField.sendKeys(userID);
		passwordField.sendKeys(password);
		submitBtn.click();
	}

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");

	}

}
