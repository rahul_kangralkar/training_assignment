package com.newtours.qaf.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class BookAFlightPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "bookAflightpage.btn.securePurchaseBtn")
	private QAFWebElement SecurePurchaseBtn;

	public FlightConfirmationPage gotoFlightConfirmationPage() {
		SecurePurchaseBtn.click();
		FlightConfirmationPage confirm = new FlightConfirmationPage();
		confirm.waitForPageToLoad();
		return confirm;
	}

	public QAFWebElement getSecurePurchaseBtn() {
		return SecurePurchaseBtn;
	}

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");

	}

}
