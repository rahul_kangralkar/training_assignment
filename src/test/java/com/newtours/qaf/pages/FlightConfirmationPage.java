package com.newtours.qaf.pages;

import java.util.List;

import org.openqa.selenium.By;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class FlightConfirmationPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "flightconfirmationpage.btn.LogoutBtn")
	private QAFWebElement LogoutBtn;

	public QAFWebElement getLogoutBtn() {
		return LogoutBtn;
	}

	public SignOnPage gotoSignOnPage() {
		/*List<QAFWebElement> list = driver.getElements(By.xpath("//a/img"));
		for (QAFWebElement qafWebElement : list) {
			if (qafWebElement.getAttribute("src").contains("forms/Logout.gif")) {
				qafWebElement.click();
			}
		}*/
		LogoutBtn.click();
		
		return new SignOnPage();
	}

	public void verifyPageTitle() {
		Validator.assertTrue(driver.getTitle().equals("Flight Confirmation: Mercury Tours"), "Title is incorrect",
				"Title is correct");
		Reporter.log("title of the page is " + driver.getTitle());
	}

	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		driver.get("/");

	}

}
