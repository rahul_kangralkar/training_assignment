package com.newtours.qaf.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class FlightFinderPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "flightfinderpage.radioBtn.roundTripBtn")
	private QAFWebElement RoundTripBtn;

	@FindBy(locator = "flightfinderpage.btn.continueBtn")
	private QAFWebElement ContinueBtn;

	public SelectFlightsPage gotoSelectFlightsPage() {
		ContinueBtn.click();
		return new SelectFlightsPage();

	}

	public QAFWebElement getContinueBtn() {
		return ContinueBtn;
	}

	public void verifyRoundTripBtn() {

		Validator.assertTrue(RoundTripBtn.verifyPresent(), "RoundTripBtn is not present", "RoundTripBtn is present");
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
	}

}
